# Simple AXI4-Stream -> PCIe core for Virtex 7 #
This is a simple implementation of a core that transfers the data from AXI4-Stream interface to the PC via PCIe interface.
It also provides the AXI4-Lite interface for control registers implemented in FPGA.

The project is built using the VEXTPROJ environment available at https://github.com/wzab/vextproj
and described in http://dx.doi.org/10.1117/12.2247944
